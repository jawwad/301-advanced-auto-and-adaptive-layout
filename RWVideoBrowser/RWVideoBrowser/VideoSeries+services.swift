/*
* Copyright (c) 2016 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation

enum ParseError: ErrorType {
  case JSONError
}

extension VideoSeries {
  static func getAllSeries(completionHandler: ([VideoSeries]) -> Void) {
    let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)
    dispatch_async(backgroundQueue) {
      var videoSeries = [VideoSeries]()
      let filePath = NSBundle.mainBundle().pathForResource("VideoInfo", ofType: "json")!
      let data = NSData(contentsOfFile: filePath)!
      guard let seriesDictionaries = try? self.parseJSON(data) else {
        fatalError("Error parsing json data")
      }
      for seriesDictionary in seriesDictionaries {
        let series = VideoSeries(dictionary: seriesDictionary)
        videoSeries.append(series)
      }
      dispatch_async(dispatch_get_main_queue()) {
        completionHandler(videoSeries)
      }
    }
  }

  private static func parseJSON(data: NSData) throws -> [[String: AnyObject]] {
    do {
      let parseResults = try NSJSONSerialization.JSONObjectWithData(data, options: [])
      if let array = parseResults as? [[String: AnyObject]] {
        return array
      }
    }
    throw ParseError.JSONError
  }
}
