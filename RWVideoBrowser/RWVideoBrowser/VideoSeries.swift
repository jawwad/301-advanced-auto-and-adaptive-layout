/*
* Copyright (c) 2016 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation

struct VideoSeries {
  let title: String
  let description: String
  let author: String
  let videos: [Video]

  init(dictionary: [String: AnyObject]) {
    guard let seriesTitle = dictionary["title"] as? String else {
      fatalError("Error parsing series title")
    }
    title = seriesTitle

    guard let seriesDescription = dictionary["description"] as? String else {
      fatalError("Error parsing series description")
    }
    description = seriesDescription

    guard let seriesAuthor = dictionary["author"] as? String else {
      fatalError("Error parsing series author")
    }
    author = seriesAuthor

    guard let videosArray = dictionary["videos"] as? [[String: String]] else {
      fatalError("Error parsing series videos")
    }
    var seriesVideos = [Video]()
    for videoDictionary in videosArray {
      let video = Video(dictionary: videoDictionary)
      seriesVideos.append(video)
    }
    videos = seriesVideos
  }
}
